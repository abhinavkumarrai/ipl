const path = require('path');
const ipl = require('./ipl');
const { readFromCSV, writeToJSON } = require('./util/index');

const filePath = {
  'matches': path.join(__dirname, '../data/matches.csv'),
  'deliveries': path.join(__dirname, '../data/deliveries.csv'),
};

const outputPath = {
  'matchesPerYear': path.join(__dirname, '../public/output/matchesPerYear.json'),
  'matcheswonperteam': path.join(__dirname, '../public/output/matcheswonperteam.json'),
  'extrarun2016': path.join(__dirname, '../public/output/extrarun2016.json'),
  'economicalbowler': path.join(__dirname, '../public/output/economicalbowler.json'),
  

  'deliveries': path.join(__dirname, '../data/deliveries.csv'),
};

async function main() {
  const matchesJSON = await readFromCSV(filePath.matches);
  const deliveriesJSON = await readFromCSV(filePath.deliveries);

  // Question 1
  const matchesPerYear = ipl.Q1(matchesJSON);
  writeToJSON(outputPath.matchesPerYear, matchesPerYear);

  // Question 2
   const matcheswonperteam = ipl.Q2(matchesJSON);
   writeToJSON(outputPath.matcheswonperteam, JSON.stringify(matcheswonperteam));

  // Question 3
   const extrarun2016 = ipl.Q3(matchesJSON,deliveriesJSON);
   writeToJSON(outputPath.extrarun2016, JSON.stringify(extrarun2016));

  // Question 4
   const economicalbowler = ipl.Q4(matchesJSON,deliveriesJSON);
  writeToJSON(outputPath.economicalbowler, JSON.stringify(economicalbowler));

}

main();
