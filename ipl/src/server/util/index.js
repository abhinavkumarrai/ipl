const csv2json = require('csvtojson');
const fs = require('fs');

async function readFromCSV(filePath) {
  const json = await csv2json().fromFile(filePath);
  return json;
}
async function writeToJSON(filePath, json) {
  fs.writeFileSync(filePath, JSON.stringify(json));
}

module.exports = {
  readFromCSV,
  writeToJSON,
}