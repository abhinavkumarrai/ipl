function findid(matches,year){
return matches.filter(obj => obj.season == year).map(obj2 => obj2.id);
}


function extrarun2016(matches,deliveries) {
  const ids = findid(matches, 2016);
  const delin2016 = deliveries.filter(obj3 => ids.indexOf(obj3.match_id) !== -1);
  const result = delin2016.reduce((a, b) => {
      if (a.hasOwnProperty(b.bowling_team)) {
          a[b.bowling_team] += +b.extra_runs;
      } else {
          a[b.bowling_team] = +b.extra_runs;
      }
      return a;
  }, {})

  return result;
}


module.exports = extrarun2016;