function Q1(matches) {
  let result = {};
  matches.forEach(match => {
    const year = match.season;
    if (result.hasOwnProperty(year)) {
      result[year] += 1;
    } else {
      result[year] = 1;
    }
  });
  return result;
}
module.exports = Q1;