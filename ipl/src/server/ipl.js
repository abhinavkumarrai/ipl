const Q1 = require('./question/q1');
const Q2 = require('./question/q2');
const Q3 = require('./question/q3');
const Q4 = require('./question/q4');

module.exports = {
  Q1,
  Q2,
  Q3,
  Q4,
};